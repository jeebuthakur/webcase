package web;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.util.Assert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class youcomerse {

	
	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Thakur");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0.0");
		dc.setCapability("appPackage", "com.androidsample.generalstore");
		dc.setCapability("appActivity", "com.androidsample.generalstore.SplashActivity");
		
		AndroidDriver<AndroidElement>driver=new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),dc);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//driver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		
		driver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Brazil\"))").click();
		driver.findElementByAndroidUIAutomator("text(\"Enter name here\")").sendKeys("Rajeev");
		driver.hideKeyboard();//For Hide Keyboard.
		driver.findElementByAndroidUIAutomator("text(\"Female\")").click();
		driver.findElementByAndroidUIAutomator("text(\"Let's  Shop\")").click();
		//String toastmessage=driver.findElementByXPath("//android.widget.Toast[1]").getAttribute("name");
		//Toast massage ko handle kar ne ke leye//("//android.widget.Toast[1]")ye vala path predefind hota hai.
		//System.out.println(toastmessage);
		//Assert.assertEquals("Please enter your name", toastmessage);//Actual validation.
		//Name attribute for toast message will have context.
		//driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Jordan Lift Off\"))");
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.androidsample.generalstore:id/rvProductList\")).scrollIntoView(new UiSelector().textMatches(\"Jordan Lift Off\").instance(0))");                                         
		int count= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).size();
		
		for (int i=0;i<count;i++)
		{
			String text= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).get(i).getText();
			if (text.equalsIgnoreCase("Jordan Lift Off"))  //if (text=="Jordan Lift Off") ese bhi likh sakte hai.
			{
				driver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(i).click();
				break;
			}
			
		}
		
		driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		String lastpageText=    driver.findElement(By.id("com.androidsample.generalstore:id/productName")).getText();

		//Assert.assert("Jordan 6 Rings", lastpageText);
}
}