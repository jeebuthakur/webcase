package web;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.util.Assert;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class youcomerseSelectMultipleProduct {

	
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Thakur");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0.0");
		dc.setCapability("appPackage", "com.androidsample.generalstore");
		dc.setCapability("appActivity", "com.androidsample.generalstore.SplashActivity");
		
		AndroidDriver<AndroidElement>driver=new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),dc);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Brazil\"))").click();
		driver.findElementByAndroidUIAutomator("text(\"Enter name here\")").sendKeys("Rajeev");
		driver.hideKeyboard();//For Hide Keyboard.
		driver.findElementByAndroidUIAutomator("text(\"Female\")").click();
		driver.findElementByAndroidUIAutomator("text(\"Let's  Shop\")").click();
		//select Multiple Item to add cart.
		driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		Thread.sleep(4000);
		//Validate Price--Total amout of mutiple item showing is correct .
		String amount1= driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(0).getText();
		//Need to remove Currency Sign like $,rs etc.  $120.0.
		amount1 =amount1.substring(1);
		double amount1Value= Double.parseDouble(amount1);//point me hai value to hum double user kare ge. or ye method hota hai chnage karne ka.ok
		//is ke baad keval 120.0 dikhe ga.
		String amount2= driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(1).getText();
		amount2 =amount2.substring(1);// Out Put Showing 120+120=240.
		double amount2Value= Double.parseDouble(amount2);
		double SumofTotalAmount=amount1Value+amount2Value;
		System.out.println(SumofTotalAmount+"Total Amount of the Products which we are add in Cart");
		
		String total= driver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
		total=total.substring(1);
		double totalValue=Double.parseDouble(total);
		System.out.println(totalValue+"Total Value of the Products which we are add in Cart");
		//Assert.assertEquals(SumofTotalAmount, totalValue);
		driver.findElementByAndroidUIAutomator("text(\"Send me e-mails on discounts related to selected products in future\")").click();
		//driver.findElementByAndroidUIAutomator("text(\"Please read our terms of conditions\")").click();
		TouchAction t=new TouchAction(driver);
		WebElement pn= driver.findElementByXPath("//*[@text='Please read our terms of conditions']");
		t.longPress(longPressOptions().withElement(element(pn)).withDuration(ofSeconds(2))).release().perform();
		driver.findElementByXPath("//*[@text='CLOSE']").click();
		driver.findElementById("com.androidsample.generalstore:id/btnProceed").click();
}
}