package web;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.WebElement;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.offset.ElementOption.element;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.MobileCapabilityType;


public class youcomerseWebView {

	
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Thakur");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0.0");
		dc.setCapability("appPackage", "com.androidsample.generalstore");
		dc.setCapability("appActivity", "com.androidsample.generalstore.SplashActivity");
		
		AndroidDriver<AndroidElement>driver=new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),dc);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Brazil\"))").click();
		driver.findElementByAndroidUIAutomator("text(\"Enter name here\")").sendKeys("Rajeev");
		driver.hideKeyboard();//For Hide Keyboard.
		driver.findElementByAndroidUIAutomator("text(\"Female\")").click();
		driver.findElementByAndroidUIAutomator("text(\"Let's  Shop\")").click();
		
		driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		Thread.sleep(4000);
		//driver.findElementByAndroidUIAutomator("text(\"Send me e-mails on discounts related to selected products in future\")").click();
		//TouchAction t=new TouchAction(driver);
		//WebElement pn= driver.findElementByXPath("//*[@text='Please read our terms of conditions']");
		//t.longPress(longPressOptions().withElement(element(pn)).withDuration(ofSeconds(2))).release().perform();
		//driver.findElementByXPath("//*[@text='CLOSE']").click();
		driver.findElementById("com.androidsample.generalstore:id/btnProceed").click();
		Thread.sleep(7000);
		Set<String>contexts=driver.getContextHandles();//Kon Kon se context hai matlab app+web view hai ki nahi.
		for (String contextName :contexts)
		{
		    System.out.println(contextName);
		}
		driver.context("WEBVIEW_com.androidsample.generalstore");
		//Niche logs me a jaye ga..vaha per NATIVE_APP+WEBVIEW_com.androidsample.generalstore Matlab hybrid App Hai ye.
		 driver.findElementByName("q").sendKeys("hello");
		 driver.findElementByName("q").sendKeys(Keys.ENTER);
		 driver.pressKey(new KeyEvent(AndroidKey.BACK));//Default key from android Phone Back button.
		 driver.context("NATIVE_APP");
		 
		 //Click on laptop www.google.com, then Inspect mode, click search button,find "name"-"q" keyword.
		 
		}
}